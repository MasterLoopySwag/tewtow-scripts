import random

cog_indices = [['f', 'p', 'ym', 'mm', 'ds', 'hh', 'cr', 'tbc'],
               ['bf', 'b', 'dt', 'ac', 'bs', 'sd', 'le', 'bw'],
               ['sc', 'pp', 'tw', 'bc', 'nc', 'mb', 'ls', 'rb'],
               ['cc', 'tm', 'nd', 'gh', 'ms', 'tf', 'm', 'mh']]

cogDept = random.choice(cog_indices)
cogChoice = random.choice(cogDept)
cogIndex = cogDept.index(cogChoice)
cogLevel = random.randint(cogIndex + 1, cogIndex + 5)

print("~spawnCog %s %d %d" % (cogChoice, cogLevel, random.choice([0, 1, 2])))
