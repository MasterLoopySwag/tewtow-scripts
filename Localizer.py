CogFullNames = {'f': 'Flunky',
                'p': 'Pencil Pusher',
                'mb': 'Money Bags'}

CogSpecials = {0: "Molten",
               1: "Undead",
               2: "Withered",
               3: "Slow Start",
               4: "Toxic",
               5: "Overclocked",
               6: "Executive"}

CogDeptNames = {0: "Bossbot",
                1: "Lawbot",
                2: "Cashbot",
                3: "Sellbot"}