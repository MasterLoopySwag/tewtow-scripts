import random
import Localizer


class CogSim:

    def __init__(self, name, dept, level):
        self.name = name
        self.fullName = Localizer.CogFullNames.get(name)
        self.dept = dept
        self.deptName = Localizer.CogDeptNames.get(dept)
        self.level = level
        self.maxHp = (level + 1) * (level + 2)
        self.hp = self.maxHp
        self.specials = []
        self.specialNames = []
        self.randomSpeed(self.level * 5)
        self.statusEffects = []

    def randomSpeed(self, speed):
        randomSpeed = random.choice([0, 1])
        if randomSpeed == 1:
            self.speed = random.randint(1, int(self.level * 10))
        else:
            self.speed = speed
        return

    def setHp(self, amount, isDamage = 1):
        if isDamage == 1:
            self.hp = self.hp - amount
        elif self.hp + amount > self.maxHp:
            self.maxHp = self.hp + amount
            self.hp = self.maxHp
        else:
            self.hp = self.hp + amount

    def setMaxHp(self, maxHp):
        self.maxHp = maxHp
        self.hp = maxHp

    def setSpeed(self, speed):
        self.speed = speed

    def addSpecial(self, special):
        self.specials.append(special)

    def getSpecialNames(self):
        for special in self.specials:
            self.specialNames.append(Localizer.CogSpecials.get(special))

    def setFullName(self, name):
        self.fullName = name  # This could be useful for bosses.


moneybags = CogSim('mb', 2, 15)
moneybags.addSpecial(3)
moneybags.addSpecial(5)
moneybags.addSpecial(6)
moneybags.getSpecialNames()
moneybags.setFullName('Mr. Monopoly')
moneybags.statusEffects.append('Frozen')
moneybags.statusEffects.append('Burning')

if len(moneybags.specials) == 0:
    print("%s \n%s \nLevel %d" % (moneybags.fullName, moneybags.deptName, moneybags.level))
elif len(moneybags.specials) == 1:
    print("%s \n%s \nLevel %d %s" % (moneybags.fullName, moneybags.deptName, moneybags.level, str(moneybags.specialNames[0])))
else:
    print("%s \n%s \nLevel %d Mixed" % (moneybags.fullName, moneybags.deptName, moneybags.level))
print("Current HP: %d/%d" % (moneybags.maxHp, moneybags.hp))
print("Current status effects: " + str(moneybags.statusEffects))
if len(moneybags.specials) > 1:
    print("Specials used: %s" % (str(moneybags.specialNames)))

# Notes:
# - You won't see their special if there are more than one! You will only see Mixed!
# - For mini-boss purposes, you may use specials and statuses in a creative way.
# - Slow status effect is NOT the same as Slow Start special!
